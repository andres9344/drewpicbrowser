package com.aarr.simplebrowser.Database.Dao

import com.aarr.simplebrowser.Database.Model.BlacklistFilter
import com.aarr.simplebrowser.Database.Model.Bookmarks
import com.aarr.simplebrowser.Database.Model.History

/**
 * Created by andresrodriguez on 9/4/17.
 */

class HistoryDao{

    fun findHistoryByUrl(query:String?):List<History>?{
        var lstFilters:List<History>? = null

        val qB = History.getRepository().queryBuilder()
        val w = qB.where()

        if (query!=null && !query.equals("")){
            try {
                w.or(
                        w.eq("url",query),
                        w.like("url","%"+query+"%"))
                lstFilters = qB.query()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        return lstFilters
    }

    fun findHistories():List<History>?{
        var lstFilters:List<History>? = null

        val qB = History.getRepository().queryBuilder()

        try {
            qB.orderBy("createdDate",false)
            lstFilters = qB.query()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return lstFilters
    }
}
