package com.aarr.simplebrowser.Database.Helper;

import android.app.Application;
import android.content.Context;

/**
 * Created by andresrodriguez on 11/20/16.
 */
public class DatabaseContext extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        DatabaseContext.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return DatabaseContext.context;
    }
}
