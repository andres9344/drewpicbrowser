package com.aarr.simplebrowser.Database.Model;

import com.aarr.simplebrowser.Database.Helper.DatabaseHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by andresrodriguez on 11/20/16.
 */
@DatabaseTable(tableName = "blacklist_filter")

public class BlacklistFilter {

//    Type:
//    0: Search Engines
//    1: Video Engines
//    2: Others

    @DatabaseField(generatedId = true) int id;
    @DatabaseField String url;
    @DatabaseField int idUsuario;
    @DatabaseField Date createdDate;
    @DatabaseField boolean active;
    @DatabaseField int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static RuntimeExceptionDao<BlacklistFilter, Integer> getRepository()
    {
        return DatabaseHelper.DaoGet.get(BlacklistFilter.class);
    }
}
