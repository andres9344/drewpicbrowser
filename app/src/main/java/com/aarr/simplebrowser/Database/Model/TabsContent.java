package com.aarr.simplebrowser.Database.Model;

import com.aarr.simplebrowser.Database.Helper.DatabaseHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by andresrodriguez on 11/20/16.
 */
@DatabaseTable(tableName = "tabs_content")

public class TabsContent implements Serializable {

    @DatabaseField(generatedId = true) int id;
    @DatabaseField String url;
    @DatabaseField String title;
    @DatabaseField int idUsuario;
    @DatabaseField Date createdDate;
    @DatabaseField String imgPreview;
    @DatabaseField boolean active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgPreview() {
        return imgPreview;
    }

    public void setImgPreview(String imgPreview) {
        this.imgPreview = imgPreview;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public static RuntimeExceptionDao<TabsContent, Integer> getRepository()
    {
        return DatabaseHelper.DaoGet.get(TabsContent.class);
    }
}
