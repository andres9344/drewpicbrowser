package com.aarr.simplebrowser.Database.Helper;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;


import com.aarr.simplebrowser.Database.Model.BlacklistFilter;
import com.aarr.simplebrowser.Database.Model.Bookmarks;
import com.aarr.simplebrowser.Database.Model.BrowserSettingsModel;
import com.aarr.simplebrowser.Database.Model.History;
import com.aarr.simplebrowser.Database.Model.TabsContent;
import com.aarr.simplebrowser.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 * Created by andresrodriguez on 11/20/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "SimpleBrowser.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    private static Context context;

    private String[] videoEngines = {
            "youtube.com",
            "aol.com",
            "mefeedia.com",
            "blinkx.com",
            "veoh.com",
            "pornhub.com",
            "youporn.com",
            "redtube.com",
            "xvideos.com",
            "sextube.com",
            "vimeo.com"};
    private String[] searchEngines = {
            "google.com",
            "bing.com",
            "search.yahoo.com",
            "dmoz.org",
            "excitedirectory.com",
            "whatuseek.com",
            "jayde.com",
            "secretsearchenginelabs.com",
            "exactseek.com",
            "addurl.altavista.com",
            "activesearchresults.com",
            "scrubtheweb.com",
            "fybersearch.com",
            "anoox.com",
            "searchsight.com",
            "somuch.com",
            "ghetosearch.com",
            "freeprwebdirectory.com",
            "webworldindex.com",
            "viesearch.com",
            "a1webdirectory.org",
            "search.sonicrun.com",
            "mastermoz.com",
            "onemission.com",
            "linkcentre.com",
            "directory-free.com",
            "info-listings.com",
            "onlinesociety.org",
            "infotiger.com",
            "elitesitesdirectory.com",
            "surfsafely.com",
            "businessseek.biz",
            "onemilliondirectory.com",
            "illumirate.com",
            "1websdirectory.com",
            "synergy-directory.com",
            "gigablast.com",
            "piseries.com",
            "intelseek.com",
            "kiwidir.com",
            "submit.biz",
            "247webdirectory.com",
            "blackabsolute.com",
            "9sites.net",
            "rdirectory.net",
            "nexusdirectory.com",
            "sitelistings.net",
            "amfibi.com",
            "priordirectory.com",
            "ezistreet.com",
            "gainweb.org",
            "directoryfire.com",
            "triplewdirectory.com",
            "polypat.org",
            "exalead.com",
            "towersearch.com",
            "splatsearch.com",
            "addurl.amfibi.com",
            "ghetosearch.com",
            "feedplex.com",
            "surfsafely.com",
            "searchsight.com",
            "claymont.com",
            "boitho.com",
            "websquash.com"};

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, BlacklistFilter.class);
            TableUtils.createTable(connectionSource, Bookmarks.class);
            TableUtils.createTable(connectionSource, History.class);
            TableUtils.createTable(connectionSource, BrowserSettingsModel.class);
            TableUtils.createTable(connectionSource, TabsContent.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, BlacklistFilter.class, true);
            TableUtils.createTable(connectionSource, Bookmarks.class);
            TableUtils.createTable(connectionSource, History.class);
            TableUtils.createTable(connectionSource, BrowserSettingsModel.class);
            TableUtils.createTable(connectionSource, TabsContent.class);
            onCreate(database,connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop database", e);
            throw new RuntimeException(e);
        }
    }

    public static class DaoGet<T>
    {
        public static <T> RuntimeExceptionDao<T, Integer> get(Class<T> clazz)
        {
            return DatabaseHelper.getHelper().getRuntimeExceptionDao(clazz);
        }
    }

    public static DatabaseHelper getHelper()
    {
        return new DatabaseHelper(context);
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    public void BD_backup() throws IOException {
//        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());

        final String inFileName = "/data/data/"+getContext().getPackageName()+"/databases/"+DATABASE_NAME;
        File dbFile = new File(inFileName);
        FileInputStream fis = null;

        fis = new FileInputStream(dbFile);

        String directorio = Environment.getExternalStorageDirectory()+"/Database";
        File d = new File(directorio);
        if (!d.exists()) {
            d.mkdir();
        }
        String outFileName = directorio + "/"+DATABASE_NAME;

        OutputStream output = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }

        output.flush();
        output.close();
        fis.close();

        sendDB(outFileName);

    }

    public void sendDB(String zipLocation){
        Intent itSend = new Intent(android.content.Intent.ACTION_SEND);

        itSend.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ "andresdrgarcia@gmail.com"});
        itSend.putExtra(android.content.Intent.EXTRA_BCC, "");
        itSend.putExtra(android.content.Intent.EXTRA_SUBJECT, "Backupa database");
        itSend.putExtra(android.content.Intent.EXTRA_TEXT, "Database backup sended by Netkiosk Parva");

        File dbFile = new File(zipLocation);

        itSend.setType("text/plain");
        Log.d("_______________", "as "+ Uri.fromFile(dbFile));
        itSend.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(dbFile));

        context.startActivity(Intent.createChooser(itSend, "Backup Database"));
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
