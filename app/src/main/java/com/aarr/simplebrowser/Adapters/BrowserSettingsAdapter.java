package com.aarr.simplebrowser.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aarr.simplebrowser.Database.Model.BrowserSettingsModel;
import com.aarr.simplebrowser.Database.Model.History;
import com.aarr.simplebrowser.R;
import com.aarr.simplebrowser.Views.BrowserSettings;
import com.aarr.simplebrowser.Views.MainActivity;
import com.aarr.simplebrowser.Views.SettingsActivity;
import com.aarr.simplebrowser.Views.UrlFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class BrowserSettingsAdapter extends RecyclerView.Adapter<BrowserSettingsAdapter.SettingsViewHolder> {

    private Context adapterContext;
    private List<String> settings = null;
    private List<String> info = null;
    private List<Integer> images = null;
    private Dialog dialog = null;
    private SettingsActivity activity = null;

    public List<String> getSettings() {
        if (settings==null)
            settings = new ArrayList<String>();
        return settings;
    }

    public void setSettings(List<String> settings) {
        this.settings = settings;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public List<String> getInfo() {
        return info;
    }

    public void setInfo(List<String> info) {
        this.info = info;
    }

    public static class SettingsViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageSettings;
        public TextView nameSettings;
        public TextView infoSettings;
        public LinearLayout linearLayout;

        public SettingsViewHolder(View v) {
            super(v);
            imageSettings = (ImageView) v.findViewById(R.id.imageSettings);
            nameSettings = (TextView) v.findViewById(R.id.nameSettings);
            infoSettings = (TextView) v.findViewById(R.id.infoSettings);
            linearLayout = (LinearLayout) v.findViewById(R.id.layout);
        }
    }

    public BrowserSettingsAdapter(String[] items, Integer[] images, String[] info, Context context) {
        if (items != null)
            setSettings(new ArrayList<String>(Arrays.asList(items)));
        if (images != null)
            setImages(new ArrayList<Integer>(Arrays.asList(images)));
        if (info != null)
            setInfo(new ArrayList<String>(Arrays.asList(info)));
        Log.v("SettingsSize", String.valueOf(getSettings().size()));
        adapterContext = context;
//        activity = (SettingsActivity) adapterContext;

    }

    @Override
    public BrowserSettingsAdapter.SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_settings, parent, false);
        return new SettingsViewHolder(v);
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final BrowserSettingsAdapter.SettingsViewHolder holder, final int position) {
        if (getSettings()!=null){
            if (getSettings().size()>0){
                holder.imageSettings.setImageDrawable(ContextCompat.getDrawable(adapterContext,getImages().get(position)));
                holder.nameSettings.setText(getSettings().get(position));
                holder.infoSettings.setText(getInfo().get(position));

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i;
                        switch (position){
                            case 0:
                                final Dialog dialog = new Dialog(adapterContext);
                                dialog.setContentView(R.layout.custom_input_text);

                                BrowserSettingsModel settings = null;
                                try{
                                    settings = new BrowserSettingsModel().getRepository().queryForAll().get(0);
                                }catch(Exception e){
                                    Log.v("SettingsException",e.toString());
                                }

                                final EditText input = (EditText) dialog.findViewById(R.id.inputField);
                                if (settings!=null && settings.getHomePage()!=null){
                                    input.setText(settings.getHomePage());
                                }
                                Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
                                final BrowserSettingsModel finalSettings = settings;
                                btnSave.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        String url = input.getText().toString();
                                        if (url!=null && url.equals("")){
                                            if (finalSettings !=null && url.equals(finalSettings.getHomePage())){
                                                finalSettings.setHomePage(url);
                                                new BrowserSettingsModel().getRepository().update(finalSettings);
                                            }
                                        }
                                    }
                                });
                                Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
                                btnClose.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                                break;
                            case 1:
                                AlertDialog.Builder dialog2 = new AlertDialog.Builder(adapterContext);
                                dialog2.setMessage("Are you sure you want to clear browser history?");
                                dialog2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        List<History> histories = new History().getRepository().queryForAll();
                                        if (histories!=null && histories.size()>0){
                                            new History().getRepository().delete(histories);
                                        }
                                        freeMemory();
                                        Toast.makeText(adapterContext,"History cleared!",Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }
                                });
                                dialog2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog2.show();
                                break;
                        }
                    }
                });
            }
        }

    }

    public void freeMemory(){
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    @Override
    public int getItemCount() {
        return getSettings().size();
    }
}
