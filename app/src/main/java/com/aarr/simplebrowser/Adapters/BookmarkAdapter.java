package com.aarr.simplebrowser.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aarr.simplebrowser.Database.Model.BlacklistFilter;
import com.aarr.simplebrowser.Database.Model.Bookmarks;
import com.aarr.simplebrowser.R;
import com.aarr.simplebrowser.Views.MainActivity;
import com.aarr.simplebrowser.Views.UrlFilter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarksViewHolder> {

    private Context adapterContext;
    private List<Bookmarks> bookmarks = null;
    private Dialog dialog = null;
    private MainActivity activity = null;
    private int type;

    public List<Bookmarks> getBookmarks() {
        if (bookmarks==null)
            bookmarks = new ArrayList<Bookmarks>();
        return bookmarks;
    }

    public void setBookmarks(List<Bookmarks> Bookmarks) {
        this.bookmarks = Bookmarks;
    }

    public static class BookmarksViewHolder extends RecyclerView.ViewHolder {

//        public TextView idField;
        public TextView url;
        public TextView title;
        public LinearLayout linearLayout;
        public Spinner protocolSpinner;

        public BookmarksViewHolder(View v) {
            super(v);
//            idField = (TextView) v.findViewById(R.id.idField);
            url = (TextView) v.findViewById(R.id.url);
            title = (TextView) v.findViewById(R.id.title);
            linearLayout = (LinearLayout) v.findViewById(R.id.layout);

        }
    }

    public BookmarkAdapter(List<Bookmarks> items, Context context) {
        setBookmarks(items);
        Log.v("BookmarksSize", String.valueOf(getBookmarks().size()));
        adapterContext = context;
        this.type = type;
    }

    @Override
    public BookmarkAdapter.BookmarksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);
        activity = (MainActivity) adapterContext;
        return new BookmarksViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final BookmarkAdapter.BookmarksViewHolder holder, final int position) {
        if (getBookmarks()!=null){
            if (getBookmarks().size()>0){
//                holder.idField.setText("ID: " + String.valueOf(getBookmarks().get(position).getId()));
                holder.url.setText(getBookmarks().get(position).getUrl().trim());
                holder.title.setText(getBookmarks().get(position).getTitle().trim());

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (activity!=null){
                            activity.loadUrlFromAdapter(getBookmarks().get(position).getUrl().trim());
                        }
                    }
                });
            }
        }

    }

    public String getCleanUrl(String url){
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.www\\.)","");
        return url;
    }

    @Override
    public int getItemCount() {
        return getBookmarks().size();
    }
}
