package com.aarr.simplebrowser.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aarr.simplebrowser.Database.Model.History;
import com.aarr.simplebrowser.Database.Model.TabsContent;
import com.aarr.simplebrowser.R;
import com.aarr.simplebrowser.Views.MainActivity;
import com.aarr.simplebrowser.Views.TabsActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresrodriguez on 11/21/16.
 */
public class TabsAdapter extends RecyclerView.Adapter<TabsAdapter.TabsViewHolder> {

    private Context adapterContext;
    private List<TabsContent> tabsContent = null;
    private Dialog dialog = null;
    private TabsActivity activity = null;
    private int type;

    public List<TabsContent> getTabsContent() {
        if (tabsContent==null)
            tabsContent = new ArrayList<TabsContent>();
        return tabsContent;
    }

    public void setTabsContent(List<TabsContent> tabsContent) {
        this.tabsContent = tabsContent;
    }

    public static class TabsViewHolder extends RecyclerView.ViewHolder {

//        public TextView idField;
        public TextView tabTitle;
        public ImageView tabPreview;
        public ImageView btnClose;
        public RelativeLayout tabContent;

        public TabsViewHolder(View v) {
            super(v);
//            idField = (TextView) v.findViewById(R.id.idField);
            tabTitle = (TextView) v.findViewById(R.id.tabTitle);
            btnClose = (ImageView) v.findViewById(R.id.btnClose);
            tabPreview = (ImageView) v.findViewById(R.id.tabPreview);
            tabContent = (RelativeLayout) v.findViewById(R.id.tabContent);

        }
    }

    public TabsAdapter(List<TabsContent> items, Context context) {
        setTabsContent(items);
        Log.v("TabsContentSize", String.valueOf(getTabsContent().size()));
        adapterContext = context;
    }

    @Override
    public TabsAdapter.TabsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tab, parent, false);
        activity = (TabsActivity) adapterContext;
        return new TabsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TabsAdapter.TabsViewHolder holder, final int position) {
        if (getTabsContent()!=null){
            if (getTabsContent().size()>0){
                if (getTabsContent().get(position).isActive()){
                    final int version = Build.VERSION.SDK_INT;
                    if (version >= 21) {
                        holder.tabContent.setBackground(ContextCompat.getDrawable(adapterContext, R.drawable.border_accent));
                    } else {
                        holder.tabContent.setBackground(adapterContext.getResources().getDrawable(R.drawable.border_accent));
                    }
                }
                holder.btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.deleteTab(getTabsContent().get(position));
                    }
                });
                holder.tabTitle.setText(getTabsContent().get(position).getTitle().trim());
//                holder.url.setText(getTabsContent().get(position).getUrl().trim());

                if (getTabsContent().get(position).getImgPreview()!=null){
                    try{
                        File image = new File(getTabsContent().get(position).getImgPreview());
                        if (image.exists()){
                            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
                            holder.tabPreview.setImageBitmap(bitmap);
                        }
                    }catch (Exception e){
                        Log.v("ImagenException",e.toString());
                    }

                }

                holder.tabContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    if (activity!=null){
                        activity.returnTabContent(getTabsContent().get(position));
                    }
                    }
                });
            }
        }

    }

    public String getCleanUrl(String url){
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.www\\.)","");
        return url;
    }

    @Override
    public int getItemCount() {
        return getTabsContent().size();
    }
}
