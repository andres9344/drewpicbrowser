package com.aarr.simplebrowser.Views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Adapter
import com.aarr.simplebrowser.Adapters.SettingsAdapter

import com.aarr.simplebrowser.R
import android.support.v7.widget.DividerItemDecoration



class SettingsActivity : AppCompatActivity() {

    private val settingItems = arrayOf<String>("Browser settings"
//                                                ,"Blacklist filter"
                                                )
    private val settingItemsInfo = arrayOf<String>("Browser configuration, home page, clear history, more..."
//                                                    ,"Website access control, forbidden websites"
                                                    )
    private val settingImages = arrayOf<Int>(R.mipmap.browser_64
//                                            ,R.mipmap.blacklist_64
                                            )
    private var recycler: RecyclerView? = null
    private var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        toolbar = findViewById(R.id.toolbar) as Toolbar
        toolbar!!.setTitle("Settings")
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                onBackPressed()
            }
        })

        recycler = findViewById(R.id.recycler) as RecyclerView
        fillAndResetRecycler()
    }

    fun fillAndResetRecycler(){
        recycler!!.adapter = null
        val adapter: SettingsAdapter? = SettingsAdapter(settingItems,settingImages,settingItemsInfo,this)
        val lManager : LinearLayoutManager? = LinearLayoutManager(this)
        recycler!!.layoutManager = lManager
        val dividerItemDecoration = DividerItemDecoration(recycler!!.getContext(),
                lManager!!.getOrientation())
        recycler!!.addItemDecoration(dividerItemDecoration)
        recycler!!.adapter = adapter
    }
}
