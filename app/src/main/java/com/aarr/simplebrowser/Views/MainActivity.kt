package com.aarr.simplebrowser.Views

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import com.aarr.simplebrowser.Adapters.BookmarkAdapter
import com.aarr.simplebrowser.Adapters.HistoryAdapter
import com.aarr.simplebrowser.Database.Dao.BookmarkDao
import com.aarr.simplebrowser.Database.Dao.HistoryDao
import com.aarr.simplebrowser.Database.Dao.TabsDao
import com.aarr.simplebrowser.Database.Helper.DatabaseHelper
import com.aarr.simplebrowser.Database.Model.*
import com.aarr.simplebrowser.R
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar
import com.varunest.sparkbutton.SparkButton
import com.varunest.sparkbutton.SparkEventListener
import ru.whalemare.sheetmenu.SheetMenu
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.net.MalformedURLException
import java.net.URL
import java.util.*

class MainActivity : AppCompatActivity() {

    var webView:WebView? = null
    var btnBack: ImageButton? = null
    var btnMore: ImageButton? = null
    var countTabs: TextView? = null
    var tabsContainer: RelativeLayout? = null
    var urlContainer: EditText? = null
    var progressBar: CircleProgressBar? = null
    var btnBookmark: SparkButton? = null
    var dialog: Dialog? = null
    var currentTab: TabsContent? = null
    val TAB_RESULT = 10
//    var btnCheck: FloatingActionButton? = null
    var btnCheck: RelativeLayout? = null
    var urlCheck: String = "http://drewpic.com/tumblr2/code-grabber.php?url="
    private val countries = arrayOf("", "AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW")
    private val videoEngines = arrayOf(
            "youtube.com",
            "aol.com",
            "mefeedia.com",
            "blinkx.com",
            "veoh.com",
            "pornhub.com",
            "youporn.com",
            "redtube.com",
            "xvideos.com",
            "sextube.com",
            "vimeo.com"
    )
    private val searchEngines = arrayOf(
//            "google.com",
            "bing.com",
            "search.yahoo.com",
            "dmoz.org",
            "excitedirectory.com",
            "whatuseek.com",
            "jayde.com",
            "secretsearchenginelabs.com",
            "exactseek.com",
            "addurl.altavista.com",
            "activesearchresults.com",
            "scrubtheweb.com",
            "fybersearch.com",
            "anoox.com",
            "searchsight.com",
            "somuch.com",
            "ghetosearch.com",
            "freeprwebdirectory.com",
            "webworldindex.com",
            "viesearch.com",
            "a1webdirectory.org",
            "search.sonicrun.com",
            "mastermoz.com",
            "onemission.com",
            "linkcentre.com",
            "directory-free.com",
            "info-listings.com",
            "onlinesociety.org",
            "infotiger.com",
            "elitesitesdirectory.com",
            "surfsafely.com",
            "businessseek.biz",
            "onemilliondirectory.com",
            "illumirate.com",
            "1websdirectory.com",
            "synergy-directory.com",
            "gigablast.com",
            "piseries.com",
            "intelseek.com",
            "kiwidir.com",
            "submit.biz",
            "247webdirectory.com",
            "blackabsolute.com",
            "9sites.net",
            "rdirectory.net",
            "nexusdirectory.com",
            "sitelistings.net",
            "amfibi.com",
            "priordirectory.com",
            "ezistreet.com",
            "gainweb.org",
            "directoryfire.com",
            "triplewdirectory.com",
            "polypat.org",
            "exalead.com",
            "towersearch.com",
            "splatsearch.com",
            "addurl.amfibi.com",
            "ghetosearch.com",
            "feedplex.com",
            "surfsafely.com",
            "searchsight.com",
            "claymont.com",
            "boitho.com",
            "websquash.com",
            "facebbok.com",
            "hotmail.com",
            "gmail.com",
            "freelancer.com"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            storagePermission()
        }else{
            initComponents()
        }

    }

    fun initComponents(){
        var helper: DatabaseHelper = DatabaseHelper(this)
        helper.context = this

        fillDefaultValues()

        tabsContainer = findViewById(R.id.tabsContainer) as RelativeLayout
        tabsContainer!!.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                var i: Intent? = Intent(this@MainActivity,TabsActivity::class.java)
                startActivityForResult(i,TAB_RESULT)
            }
        })
        btnBookmark = findViewById(R.id.btnBookmark) as SparkButton
        btnBookmark!!.setEventListener(object : SparkEventListener {
            override fun onEventAnimationEnd(button: ImageView?, buttonState: Boolean) {

            }

            override fun onEventAnimationStart(button: ImageView?, buttonState: Boolean) {
            }

            override fun onEvent(button: ImageView, buttonState: Boolean) {
                Log.v("CreatingBookmark","ENTER METHOD")
                if (buttonState) {
                    Log.v("CreatingBookmark","BUTTON IS ACTIVE")
                    if (existBookmark(webView!!.url)==null){
                        Log.v("CreatingBookmark","Creating: "+webView!!.url)
                        var newBook : Bookmarks? = Bookmarks()
                        newBook!!.url =  webView!!.url
                        newBook!!.title = webView!!.title
                        newBook!!.createdDate = Date()
                        newBook!!.isActive = true
                        Bookmarks.getRepository().create(newBook)
                    }else{
                        Log.v("CreatingBookmark","IS NOT NULL")
                    }
                } else {
                    Log.v("CreatingBookmark","BUTTON IS INACTIVE")
                    // Button is inactive
                }
            }
        })
        btnCheck = findViewById(R.id.btnCheck) as RelativeLayout
        btnCheck!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                dialog = Dialog(this@MainActivity)
                dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog!!.setContentView(R.layout.custom_web_view)

                var lp: WindowManager.LayoutParams? = WindowManager.LayoutParams()
                lp!!.copyFrom(dialog!!.getWindow().getAttributes())
                lp!!.width = WindowManager.LayoutParams.MATCH_PARENT
                lp!!.height = WindowManager.LayoutParams.MATCH_PARENT

                var dialogProgressBar = dialog!!.findViewById(R.id.progressBar) as CircleProgressBar
                dialogProgressBar!!.setColorSchemeResources(R.color.colorAccent)
                var btnClose: ImageButton? = dialog!!.findViewById(R.id.btnClose) as ImageButton
                btnClose!!.setOnClickListener(object : View.OnClickListener{
                    override fun onClick(v: View?) {
                        dialog!!.dismiss()
                    }
                })
                var webViewDialog: WebView? = dialog!!.findViewById(R.id.customWebView) as WebView
                if (webViewDialog!=null){
                    webViewDialog.getSettings().setLoadWithOverviewMode(true)
                    webViewDialog.getSettings().javaScriptEnabled = true
                    webViewDialog.setVerticalScrollBarEnabled(true)
                    webViewDialog.setHorizontalScrollBarEnabled(true)
                    webViewDialog.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
//                    webViewDialog.getSettings().setUseWideViewPort(true)
//                    var currentUrl : String? = urlContainer!!.text.toString()
                    var currentUrl : String? = webView!!.url
                    Log.v("CheckUrl","1:"+currentUrl)
//                    Log.v("CheckUrl","2:"+currentUrlAux)

                    if (currentUrl!=null && !currentUrl.equals("")){
                        if (setFilteredPage(currentUrl)){
                            var fullUrl : String? = "http://drewpic.com/tumblr2/code-grabber.php?url="+removeProtocol(currentUrl)
                            Log.v("Loading",fullUrl)
                            webViewDialog!!.setWebViewClient(object: WebViewClient() {
                                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                                    super.onPageStarted(view, url, favicon)
                                }

                                override fun onPageFinished(view: WebView?, url: String?) {
                                    dialogProgressBar.visibility = View.GONE
                                    super.onPageFinished(view, url)
                                }
                            })
                            dialogProgressBar.visibility = View.VISIBLE
                            webViewDialog!!.loadUrl(fullUrl)
                        }
                    }
                }
                dialog!!.show()
                dialog!!.getWindow().setAttributes(lp)
            }
        })
        btnCheck!!.visibility = View.GONE
        urlContainer = findViewById(R.id.urlContainer) as EditText
        urlContainer!!.setOnEditorActionListener(object: TextView.OnEditorActionListener{
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    setWebView(urlContainer!!.text.toString())
                    v!!.clearFocus()
                    hideKeyboard()
                    return true
                }
                return false
            }
        })
        urlContainer!!.onFocusChangeListener = object: View.OnFocusChangeListener{
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (hasFocus){
//                    val pos = urlContainer!!.text.length
//                    urlContainer!!.setSelection(pos)
                }
            }
        }

        webView = findViewById(R.id.webView) as WebView
        webView!!.getSettings().useWideViewPort = true
        webView!!.getSettings().javaScriptEnabled = true
        webView!!.setVerticalScrollBarEnabled(true);
        webView!!.setHorizontalScrollBarEnabled(true);
        webView!!.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
        btnBack = findViewById(R.id.btnBack) as ImageButton
        btnBack!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                if (webView != null) {
                    var settingsModel: BrowserSettingsModel? = null
                    try{
                        settingsModel = BrowserSettingsModel.getRepository().queryForAll().get(0)
                    }catch(e:Exception){
                        Log.v("SettingsException",e.toString())
                    }
                    if (settingsModel !=null){
                        if (settingsModel.homePage!=null && !settingsModel.homePage.equals("")){
                            setWebView(settingsModel.homePage)
                        }
                    }
                }
            }

        })
        btnMore = findViewById(R.id.btnMore) as ImageButton
        btnMore!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                showMenu()
            }
        })
        countTabs = findViewById(R.id.countTabs) as TextView
        progressBar = findViewById(R.id.progressBar) as CircleProgressBar
        progressBar!!.setColorSchemeResources(R.color.colorAccent)

        currentTab = TabsDao().findTabActive()
        if (currentTab!=null){
            urlContainer!!.setText(currentTab!!.url)
            setWebView(currentTab!!.url)
        }

        updateTabsCount()
        showAdvice()
    }

    override fun onResume() {
//        updateTabsCount()
        super.onResume()
    }

    fun updateTabsCount(){
        val tabsCount = TabsContent.getRepository().count()
        countTabs!!.text = tabsCount.toString()
    }

    fun showMenu(){
        SheetMenu().apply {
            click = object: MenuItem.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem?): Boolean {
                    if (item!!.itemId == R.id.newTab){
                        var settingsModel: BrowserSettingsModel? = null
                        try{
                            settingsModel = BrowserSettingsModel.getRepository().queryForAll().get(0)
                        }catch(e:Exception){
                            Log.v("SettingsException",e.toString())
                        }
                        if (settingsModel !=null){
                            if (settingsModel.homePage!=null && !settingsModel.homePage.equals("")){

                                currentTab!!.isActive = false
                                TabsContent.getRepository().update(currentTab)

                                var tab: TabsContent? = TabsContent()
                                tab!!.createdDate = Date()
                                tab!!.idUsuario = 0
                                tab!!.title = "Google"
                                tab!!.url = settingsModel.homePage
                                tab!!.isActive = true
                                TabsContent.getRepository().create(tab)
                                currentTab = tab
                                urlContainer!!.setText(settingsModel.homePage)
                                setWebView(settingsModel.homePage)
                                updateTabsCount()
                            }
                        }

//                        var helper: DatabaseHelper? = DatabaseHelper(this@MainActivity)
//                        try{
//                            helper!!.BD_backup()
//                        }catch(e:Exception){
//                            Log.w("DBException",e.toString())
//                        }
                        Toast.makeText(this@MainActivity,"New Tab",Toast.LENGTH_SHORT).show()
                    }else if (item!!.itemId == R.id.favorite){
                        dialog = Dialog(this@MainActivity)
                        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog!!.setContentView(R.layout.custom_list)

                        var lp: WindowManager.LayoutParams? = WindowManager.LayoutParams()
                        lp!!.copyFrom(dialog!!.getWindow().getAttributes())
                        lp!!.width = WindowManager.LayoutParams.MATCH_PARENT
                        lp!!.height = WindowManager.LayoutParams.MATCH_PARENT

                        var recycler = dialog!!.findViewById(R.id.recycler) as RecyclerView
                        var lst:List<Bookmarks>? = BookmarkDao().findBookmarksActive()
                        var adapter: BookmarkAdapter? = BookmarkAdapter(lst,this@MainActivity)
                        recycler.layoutManager = LinearLayoutManager(this@MainActivity)
                        recycler.adapter = adapter

                        var btnClose: ImageButton? = dialog!!.findViewById(R.id.btnClose) as ImageButton
                        btnClose!!.setOnClickListener(object : View.OnClickListener{
                            override fun onClick(v: View?) {
                                dialog!!.dismiss()
                            }
                        })

                        dialog!!.show()
                        dialog!!.getWindow().setAttributes(lp)
//                        var newBookmark: Bookmarks? = null
//                        var currentUrl: String? = urlContainer!!.text.toString()
//                        if (currentUrl!=null && !currentUrl.equals("")){
//                            var urlTitle: String? = ""
//                            try{
//                                urlTitle = webView!!.title
//                            }catch(e:Exception){
//                                Log.w("TitleException",e.toString())
//                            }
//                            try{
//                                newBookmark = Bookmarks.getRepository().queryForEq("url",urlContainer!!.text.toString()).get(0)
//                            }catch (e:Exception){
//                                Log.w("NewBookmarkException",e.toString())
//                            }
//                            if (newBookmark==null){
//                                newBookmark = Bookmarks()
//                                newBookmark.url = currentUrl
//                                newBookmark.title = urlTitle
//                                newBookmark.isActive = true
//                                newBookmark.createdDate = Date()
//                                Bookmarks.getRepository().create(newBookmark)
//                            }
//                        }
//                        Toast.makeText(this@MainActivity,"Favorite",Toast.LENGTH_SHORT).show()
                    }else if (item!!.itemId == R.id.refresh){
                        if (webView != null) {
                            progressBar!!.setVisibility(View.VISIBLE)
                            webView!!.reload()
                        }
//                        Toast.makeText(this@MainActivity,"Refresh",Toast.LENGTH_SHORT).show()
                    }else if (item!!.itemId == R.id.history){
                        dialog = Dialog(this@MainActivity)
                        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog!!.setContentView(R.layout.custom_list)

                        var lp: WindowManager.LayoutParams? = WindowManager.LayoutParams()
                        lp!!.copyFrom(dialog!!.getWindow().getAttributes())
                        lp!!.width = WindowManager.LayoutParams.MATCH_PARENT
                        lp!!.height = WindowManager.LayoutParams.MATCH_PARENT

                        var recycler = dialog!!.findViewById(R.id.recycler) as RecyclerView
                        var lst:List<History>? = HistoryDao().findHistories()
                        var adapter: HistoryAdapter? = HistoryAdapter(lst,this@MainActivity)
                        recycler.layoutManager = LinearLayoutManager(this@MainActivity)
                        recycler.adapter = adapter

                        var btnClose: ImageButton? = dialog!!.findViewById(R.id.btnClose) as ImageButton
                        btnClose!!.setOnClickListener(object : View.OnClickListener{
                            override fun onClick(v: View?) {
                                dialog!!.dismiss()
                            }
                        })

                        dialog!!.show()
                        dialog!!.getWindow().setAttributes(lp)
//                        var newBookmark: Bookmarks? = null
//                        var currentUrl: String? = urlContainer!!.text.toString()
//                        if (currentUrl!=null && !currentUrl.equals("")){
//                            var urlTitle: String? = ""
//                            try{
//                                urlTitle = webView!!.title
//                            }catch(e:Exception){
//                                Log.w("TitleException",e.toString())
//                            }
//                            try{
//                                newBookmark = Bookmarks.getRepository().queryForEq("url",urlContainer!!.text.toString()).get(0)
//                            }catch (e:Exception){
//                                Log.w("NewBookmarkException",e.toString())
//                            }
//                            if (newBookmark==null){
//                                newBookmark = Bookmarks()
//                                newBookmark.url = currentUrl
//                                newBookmark.title = urlTitle
//                                newBookmark.isActive = true
//                                newBookmark.createdDate = Date()
//                                Bookmarks.getRepository().create(newBookmark)
//                            }
//                        }
//                        Toast.makeText(this@MainActivity,"Favorite",Toast.LENGTH_SHORT).show()
//                        Toast.makeText(this@MainActivity,"Refresh",Toast.LENGTH_SHORT).show()
                    }else if (item!!.itemId == R.id.forward){
                        if (webView != null) {
                            if (webView!!.canGoForward()) {
                                btnCheck!!.visibility = View.GONE
                                progressBar!!.setVisibility(View.VISIBLE)
                                webView!!.goForward()
                            }
                        }
//                        Toast.makeText(this@MainActivity,"Forward",Toast.LENGTH_SHORT).show()
                    }else if (item!!.itemId == R.id.config){
                        var i: Intent? = Intent(this@MainActivity,SettingsActivity::class.java)
                        startActivity(i)
//                        Toast.makeText(this@MainActivity,"Config",Toast.LENGTH_SHORT).show()
                    }else if (item!!.itemId == R.id.back){
                        if (webView != null) {
                            if (webView!!.canGoBack()) {
                                btnCheck!!.visibility = View.GONE
                                progressBar!!.setVisibility(View.VISIBLE)
                                webView!!.goBack()
                            }
                        }
//                        Toast.makeText(this@MainActivity,"Forward",Toast.LENGTH_SHORT).show()
                    }
                    return false
                }
            }
            layoutManager = LinearLayoutManager(this@MainActivity)
            menu = R.menu.browser_menu
            autoCancel = true // true, by default
        }.show(this)
    }

    fun setWebView(urlValue: String) {
        Log.v("UnparsedUrlWebView", urlValue)
        var finalUrl: String = urlValue
        if (!urlValue.contains("http")){
            finalUrl = "https://"+urlValue
            urlContainer!!.setText(finalUrl)
        }
        try {
            val url = URL(finalUrl)
            if (!url.host.contains(".co")){
                var query: String = "http://www.google.com/#q="+url.host
                finalUrl = query
            }
            Log.v("UrlWebView", url.host)
        } catch (e: MalformedURLException) {
            Log.v("UrlWebViewException", e.toString())
            e.printStackTrace()
        }

        progressBar!!.setVisibility(View.VISIBLE)
        if (webView != null) {
            webView!!.setWebViewClient(object : WebViewClient() {

                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    Log.v("onPageStarted", url)
                    urlContainer!!.setText(url)
                    if (existBookmark(url)!=null){
                        btnBookmark!!.isChecked = true
                    }
                    else{
                        btnBookmark!!.isChecked = false
                    }
                    btnCheck!!.visibility = View.GONE
//                    if (isFromGoTo) {
//                        super.onPageStarted(view, url, favicon)
//                    } else {
                    progressBar!!.visibility = View.VISIBLE
                    Log.v("OriginalUrl", url)
                    super.onPageStarted(view, url, favicon)
                }

                override fun onPageFinished(view: WebView, url: String) {
                    Log.i("Process", "Finished loading URL: " + url)
                    urlContainer!!.setText(url)
                    progressBar!!.setVisibility(View.INVISIBLE)
                    createHistory(url,view.title)
                    updateTabContent(url,view.title)
                    if (existBookmark(url)!=null){
                        btnBookmark!!.isChecked = true
                    }
                    else{
                        btnBookmark!!.isChecked = false
                    }

                    val url1 = URL(url)
                    val urlValue = url1.host
                    val cleaned = getCleanUrl(urlValue)
                    Log.v("PageStartedUrl", urlValue)
                    Log.v("CleanedUrl", cleaned)
                    if (setFilteredPage("http://" + cleaned)) {
                        btnCheck!!.visibility = View.VISIBLE
                    } else if (setFilteredPage("http://www." + cleaned)) {
                        btnCheck!!.visibility = View.VISIBLE
                    } else if (setFilteredPage("https://" + cleaned)) {
                        btnCheck!!.visibility = View.VISIBLE
                    } else if (setFilteredPage("https://www." + cleaned)) {
                        btnCheck!!.visibility = View.VISIBLE
                    }

//                    isFromGoTo = false
//                    isHome = false
                }
            })
            Log.v("Loading",finalUrl)
            webView!!.loadUrl(finalUrl)
        }
    }

    fun updateTabContent(url:String?,title:String?){
        if (currentTab!=null){
            if (currentTab!!.imgPreview!=null){
                deleteImage(currentTab!!.imgPreview)
            }
            currentTab!!.url = url
            currentTab!!.title = title
            currentTab!!.imgPreview = saveImage()
            TabsContent.getRepository().update(currentTab)
        }
    }

    fun createHistory(url:String?,title:String?){
        if (url!=null && title!=null){
            var history:History? = History()
            history!!.createdDate = Date()
            history!!.url = url
            history!!.title = title
            History.getRepository().create(history)
        }
    }

    fun setFilteredPage(page: String): Boolean {
        val blacklist = BlacklistFilter.getRepository().queryForEq("active",true)
        if (blacklist!=null && blacklist.size>0){
            for (filter in blacklist) {
                Log.v("Blacklist", "In")
                var pageHost = ""
                try {
                    Log.v("Page", page)
                    val url = URL(page)
                    pageHost = url.host
                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                    Log.v("UrlException", e.toString())
                    Toast.makeText(this@MainActivity, R.string.NotValidUrl, Toast.LENGTH_SHORT).show()
                    return false
                }

                if (pageHost.contains("www.")){
                    try{
                        pageHost = pageHost.replace("www.","")
                    }catch(e:Exception){
                        Log.w("RemoveWWWException",e.toString())
                    }
                }
                Log.v("UrlCompareOut", pageHost + " = " + filter.getUrl())
                if (pageHost == filter.getUrl()) {
                    Log.v("UrlCompareIn", pageHost + " = " + filter.getUrl())
//                    Toast.makeText(this, R.string.ForbiddenPage, Toast.LENGTH_SHORT).show()
                    return false

                } else {
                    for (country in countries) {
                        var auxUrl = pageHost
                        var auxFilter = filter.getUrl()
                        if (country !== "") {
                            auxUrl += "." + country.toLowerCase()
                            auxFilter += "." + country.toLowerCase()
                        }
                        if (auxUrl == filter.getUrl()) {
                            Log.v("UrlCompareWhiteHost", auxUrl + " = " + filter.getUrl())
                            Log.v("UrlCompareCountryWhite2", auxUrl + " = " + filter.getUrl())
//                            Toast.makeText(this, R.string.ForbiddenPage, Toast.LENGTH_SHORT).show()
                            return false
                        } else if (pageHost == auxFilter) {
                            Log.v("UrlCompareWhiteFilter", pageHost + " = " + auxFilter)
                            Log.v("UrlCompareCountryWhite2", pageHost + " = " + auxFilter)
//                            Toast.makeText(this, R.string.ForbiddenPage, Toast.LENGTH_SHORT).show()
                            return false
                        }
                    }
                }
                //                else{
                //                    return true;
                //                }
            }
        }
//        Log.v("NoOne", "No option")
//        Toast.makeText(this, R.string.ForbiddenPage, Toast.LENGTH_SHORT).show()
        return true
    }

    fun getCleanUrl(url: String): String {
        var url = url
        url = url.replaceFirst("^(http://www\\.|http://|www\\.https://www\\.|https://|www\\.http://m\\.|http://|m\\.https://m\\.|https://|m\\.)".toRegex(), "")
        return url
    }

    fun removeProtocol(url: String): String {
        var url = url
        url = url.replaceFirst("^(http://\\.https://\\./\\.)".toRegex(), "")
        return url
    }

    fun fillDefaultValues() {
        try {
            val lst = BlacklistFilter.getRepository().queryForAll()
            if (lst == null || lst!!.size <= 0) {
                for (search in searchEngines) {
                    val newFilter = BlacklistFilter()
                    newFilter.isActive = true
                    newFilter.createdDate = Date()
                    newFilter.idUsuario = 0
                    newFilter.type = 0
                    newFilter.url = search
                    BlacklistFilter.getRepository().create(newFilter)
                }
                for (video in videoEngines) {
                    val newFilter = BlacklistFilter()
                    newFilter.isActive = true
                    newFilter.createdDate = Date()
                    newFilter.idUsuario = 0
                    newFilter.type = 1
                    newFilter.url = video
                    BlacklistFilter.getRepository().create(newFilter)
                }
            }

            var settings:BrowserSettingsModel? = null
            try{
                settings = BrowserSettingsModel.getRepository().queryForAll().get(0)
            }catch(e:Exception){
                Log.v("Exception",e.toString())
            }
            if (settings==null){
                val newSettings = BrowserSettingsModel()
                newSettings.homePage = "www.google.com"
                newSettings.isShowAdvice = true
                BrowserSettingsModel.getRepository().create(newSettings)
            }

            val lst2 = TabsContent.getRepository().queryForAll()
            if (lst2 == null || lst2!!.size <= 0) {

                var settingsModel: BrowserSettingsModel? = null
                try{
                    settingsModel = BrowserSettingsModel.getRepository().queryForAll().get(0)
                }catch(e:Exception){
                    Log.v("SettingsException",e.toString())
                }
                if (settingsModel !=null){
                    if (settingsModel.homePage!=null && !settingsModel.homePage.equals("")){
                        var tab: TabsContent? = TabsContent()
                        tab!!.createdDate = Date()
                        tab!!.idUsuario = 0
                        tab!!.title = "Google"
                        tab!!.url = settingsModel.homePage
                        tab!!.isActive = true
                        TabsContent.getRepository().create(tab)
                        currentTab = tab
                    }
                }


            }
        } catch (e: Exception) {
            Log.w("FillDefaultException", e.toString())
        }

    }

    fun existBookmark(url:String?):Bookmarks?{
        var lst:Bookmarks? = null
        if (url!=null && !url.equals("")){
            try{
                lst = BookmarkDao().findBookmarkByUrl(url)!!.get(0)
            }catch (e:Exception){
                Log.w("Exception",e.toString())
            }
        }

        return lst
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun loadUrlFromAdapter(url:String){
        if (dialog!=null){
            urlContainer!!.setText(url)
            dialog!!.dismiss()
            setWebView(url)
        }
    }

    fun getPictureWebView():String?{
        var picture: Picture? = webView!!.capturePicture()
        var b : Bitmap? = Bitmap.createBitmap(picture!!.width,picture!!.height,Bitmap.Config.ARGB_8888)
        var c: Canvas? = Canvas(b)
        picture.draw( c )
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream( "mnt/sdcard/yahoo.jpg" )
                if ( fos != null ){
                    b!!.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                    fos.close()
                }
            }
       catch(e:Exception)
       {

       }
        return null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK){
            when(requestCode){
                TAB_RESULT ->
                        if (data!=null){
                            var tabsContent: TabsContent? = data.getSerializableExtra("tab") as TabsContent
                            if (tabsContent!=null){
                                if (tabsContent != currentTab){
                                    currentTab!!.isActive = false
                                    TabsContent.getRepository().update(currentTab)

                                    tabsContent!!.isActive = true
                                    TabsContent.getRepository().update(tabsContent)
                                    currentTab = tabsContent
                                    urlContainer!!.setText(tabsContent.url)
                                    setWebView(tabsContent.url)
                                    updateTabsCount()
                                }
                            }
                        }
            }
        }
    }

    fun saveImage(): String {
        webView!!.measure(View.MeasureSpec.makeMeasureSpec(
                                                    View.MeasureSpec.UNSPECIFIED,
                                                    View.MeasureSpec.UNSPECIFIED),
                                                    View.MeasureSpec.makeMeasureSpec(0,
                                                                    View.MeasureSpec.UNSPECIFIED))
        webView!!.layout(0, 0, webView!!.getMeasuredWidth(),webView!!.getMeasuredHeight())
        webView!!.setDrawingCacheEnabled(true)
        webView!!.buildDrawingCache()
        var bm: Bitmap? = Bitmap.createBitmap(
                webView!!.getMeasuredWidth(),
                webView!!.getMeasuredHeight()/3,
                Bitmap.Config.ARGB_8888)

        var bigcanvas: Canvas? = Canvas(bm)
        var paint: Paint? = Paint()
        var iHeight: Int? = bm!!.getHeight()
        bigcanvas!!.drawBitmap(bm, 0.0f, iHeight!!.toFloat(), paint)
        webView!!.draw(bigcanvas)

        var fOut: OutputStream? = null
        var outputFileUri = ""
        try {
            val root = File(Environment.getExternalStorageDirectory().toString()
                    + File.separator + "TabsPreview" + File.separator)
            root.mkdirs()
            val sdImageMainDirectory = File(root, getImageName())
            outputFileUri = sdImageMainDirectory.absolutePath
            fOut = FileOutputStream(sdImageMainDirectory)
        } catch (e: Exception) {
            Toast.makeText(this, "Error saving preview: "+e.toString(),
                    Toast.LENGTH_SHORT).show()
        }

        try {
            bm!!.compress(Bitmap.CompressFormat.PNG, 50, fOut)
            fOut!!.flush()
            fOut.close()
        } catch (e: Exception) {
            Log.v("CreatePreview", "Exception imagen: " + e.toString())
        }

        Log.v("CreatePreview", "Image Path: " + outputFileUri)
        return outputFileUri
    }

    fun getImageName(): String {
        val c = Calendar.getInstance()
        val DIA = c.get(Calendar.DAY_OF_MONTH).toString()
        val MES = c.get(Calendar.MONTH).toString()
        val ANO = c.get(Calendar.YEAR).toString()
        val HORA = c.get(Calendar.HOUR).toString()
        val MINUTO = c.get(Calendar.MINUTE).toString()
        val SEGUNDO = c.get(Calendar.SECOND).toString()

        val nombre = DIA + MES + ANO + "_" + HORA + MINUTO + SEGUNDO + ".png"
        Log.v("CreatePreview", "Name imagen: " + nombre)

        return nombre
    }

    fun deleteImage(path:String?){
        val fdelete = File(path)
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + path)
            } else {
                System.out.println("file not Deleted :" + path)
            }
        }
    }

    fun showAdvice(){
        var settings:BrowserSettingsModel? = null
        try{
            settings = BrowserSettingsModel.getRepository().queryForAll().get(0)
        }catch(e:Exception){
            Log.v("Exception",e.toString())
        }
        if (settings!=null){
            if (settings.isShowAdvice){
                settings.isShowAdvice = false
                BrowserSettingsModel.getRepository().update(settings)
                dialog = Dialog(this@MainActivity)
                dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog!!.setContentView(R.layout.layout_advice)

                var btnClose: ImageButton? = dialog!!.findViewById(R.id.btnClose) as ImageButton
                btnClose!!.setOnClickListener(object : View.OnClickListener{
                    override fun onClick(v: View?) {
                        dialog!!.dismiss()
                    }
                })

                dialog!!.show()

            }
        }
    }

    private val MY_PERMISSIONS_REQUEST_STORAGE = 10004
    private fun storagePermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_STORAGE)
        }else{
            initComponents()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initComponents()
            } else {
                Toast.makeText(this@MainActivity, "Storage permission not enabled, app will close", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

}
