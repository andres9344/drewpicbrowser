package com.aarr.simplebrowser.Views

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.*
import android.util.Log
import android.view.View
import com.aarr.simplebrowser.Adapters.SettingsAdapter
import com.aarr.simplebrowser.Adapters.TabsAdapter
import com.aarr.simplebrowser.Database.Dao.TabsDao
import com.aarr.simplebrowser.Database.Model.BrowserSettingsModel
import com.aarr.simplebrowser.Database.Model.TabsContent

import com.aarr.simplebrowser.R
import java.util.*

class TabsActivity : AppCompatActivity() {

    var recycler: RecyclerView? = null
    var toolbar: Toolbar? = null
    var changeTab: Boolean = false
    var currentTab: TabsContent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tabs)

        recycler = findViewById(R.id.recycler) as RecyclerView
        toolbar = findViewById(R.id.toolbar) as Toolbar
        toolbar!!.setTitle("Tabs")
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        toolbar!!.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                onBackPressed()
            }
        })

        fillAndResetRecycler(getResources().getConfiguration().orientation)
    }

    override fun onBackPressed() {
        if (changeTab){
            returnTabContent(currentTab)
        }else{
            finish()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        setContentView(R.layout.activity_tabs)
        val orientation = newConfig!!.orientation
        fillAndResetRecycler(orientation)
    }

    fun fillAndResetRecycler(orientation:Int){
        recycler!!.adapter = null
        var lst : List<TabsContent>? = null
        try{
            lst = TabsDao().findTabs()
        }catch(e:Exception){
            Log.w("TabsException",e.toString())
        }
        val adapter: TabsAdapter? = TabsAdapter(lst,this)

        when (orientation){
            Configuration.ORIENTATION_LANDSCAPE ->
                recycler!!.layoutManager = GridLayoutManager(this,2)
            Configuration.ORIENTATION_PORTRAIT ->
                recycler!!.layoutManager = LinearLayoutManager(this)
        }


        recycler!!.adapter = adapter
    }

    fun deleteTab(tab:TabsContent?){
        var tabIsActive:Boolean = false
        if (tab!!.isActive){
            tabIsActive = true
        }
        TabsContent.getRepository().delete(tab)
        if (tabsCount()>0){
            var nextTab: TabsContent? = null
            try{
                nextTab = TabsContent.getRepository().queryForAll().get(0)
            }catch(e:Exception){
                Log.w("NextTabException",e.toString())
            }
            if (nextTab!=null && tabIsActive){
                nextTab.isActive = true
                TabsContent.getRepository().update(nextTab)
                changeTab = true
                currentTab = nextTab
            }
        }else{
            var settingsModel: BrowserSettingsModel? = null
            try{
                settingsModel = BrowserSettingsModel.getRepository().queryForAll().get(0)
            }catch(e:Exception){
                Log.v("SettingsException",e.toString())
            }
            if (settingsModel !=null){
                if (settingsModel.homePage!=null && !settingsModel.homePage.equals("")){

                    var newTab: TabsContent? = TabsContent()
                    newTab!!.createdDate = Date()
                    newTab!!.idUsuario = 0
                    newTab!!.title = "Google"
                    newTab!!.url = settingsModel.homePage
                    newTab!!.isActive = true
                    TabsContent.getRepository().create(newTab)
                    changeTab = true
                    currentTab = newTab
                }
            }
        }
        fillAndResetRecycler(getResources().getConfiguration().orientation)
    }

    fun returnTabContent(tab:TabsContent?){
        val i: Intent? = Intent()
        i!!.putExtra("tab",tab)
        setResult(Activity.RESULT_OK,i)
        finish()
    }

    fun tabsCount():Int{
        val tabsCount = TabsContent.getRepository().count()
        return tabsCount
    }
}
