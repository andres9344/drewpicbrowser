package com.aarr.simplebrowser.Views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.aarr.simplebrowser.R

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
    }
}
